  <footer class="app-footer">
      <div>
          <a href="https://coreui.io">CoreUI</a>
          <span>&copy; 2018 creativeLabs.</span>
      </div>
      <div class="ml-auto">
          <span>Powered by</span>
          <a href="https://coreui.io">CoreUI</a>
      </div>
  </footer>
  <!-- CoreUI and necessary plugins-->
  <script src="{!! asset('theme_includes/js/dist/jquery.min.js') !!}"></script>
  <script src="{!! asset('theme_includes/js/dist/popper.min.js') !!}"></script>
  <script src="{!! asset('theme_includes/js/dist/bootstrap.min.js') !!}"></script>
  <script src="{!! asset('theme_includes/js/dist/pace.min.js')!!}"> </script>
  <script src="{!! asset('theme_includes/js/dist/perfect-scrollbar.min.js') !!}"></script>
  <script src="{!! asset('theme_includes/js/dist/coreui.min.js') !!}"></script>
  <!-- Plugins and scripts required by this view-->
  <script src="{!! asset('theme_includes/js/dist/Chart.min.js') !!}"></script>
  <script src="{!! asset('theme_includes/js/dist/custom-tooltips.min.js') !!}"></script>
  <script src="{!! asset('theme_includes/js/dist/main.js ') !!}"></script>
  </body>

  </html>