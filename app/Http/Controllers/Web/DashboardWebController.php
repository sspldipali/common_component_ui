<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\CommonUIController;

class DashboardWebController extends CommonUIController
{
    //
    /**
     * This method is used to show the dashboad view
     */
    public function index()
    {
        return view('dashboard.dashboard',  $this->data);
    }
}
